import React, { useState, useEffect } from "react";
import config from "visual-config-exposer";

import { connectToParent } from "penpal";
const username = process.env["userid"] || "userid";
const userPublicAddress =
  process.env["userPublicAddress"] || "userPublicAddress";
let connection;

console.log("app NODE_ENV", process.env.NODE_ENV);

function isLoadedWithinIframe() {
  try {
    // verify the access to window.top object is succesfull.
    window.top.origin;
    return false;
  } catch (e) {
    console.log("window.top error");
    return process.env.NODE_ENV === "production";
  }
}

if (isLoadedWithinIframe()) {
  connection = connectToParent({
    // Methods child is exposing to parent
    methods: {},
  });
}

const App = () => {
  const lock_des = "./assets/lock.png";
  const [lock, setLock] = useState(lock_des);
  const lock_des2 = "./assets/lockk.png";
  const lock_des3 = "./assets/lockkk.png";
  const Image = config.settings.image;
  const Price = config.settings.price;
  useEffect(() => {
    document.getElementsByClassName("container")[0].style.backgroundImage =
      "url(" + Image + ")";
  });

  function unblur() {
    function revealAsset() {
      setTimeout(() => {
        document.getElementsByClassName("img")[0].classList.add("img1");
        document.getElementsByClassName("img")[0].classList.remove("img");
        document.getElementsByClassName("Main")[0].style.display = "none";
      }, 500);

      setTimeout(() => {
        /*var match =  window.matchMedia("(max-width: 800px)")*/
        setLock(lock_des2);
        /*if(match.matches) {
      console.log('hello')
      document.getElementsByTagName('img')[0].style.display='visible'
    }*/
      }, 1000);

      setTimeout(() => {
        setLock(lock_des3);
        document.getElementsByTagName("img")[0].classList.add("animat");
      }, 1500);

      setTimeout(() => {
        document.getElementsByClassName("container")[0].style.filter =
          "blur(0px) ";
        document.getElementsByClassName("Main")[0].style.visibility = "hidden";
        document.getElementsByTagName("img")[0].style.display = "none";
      }, 2000);
    }

    if (isLoadedWithinIframe()) {
      let transactionData = {
        amount: config.settings.price,
        recipient: {
          publicAddress: userPublicAddress,
          user: {
            name: username,
          },
        },
        metadata: {
          show: { MEMO: config.settings.headline },
        },
      };
      connection.promise.then((parent) => {
        parent
          .initiateTransaction(transactionData)
          .then((accept) => {
            console.log("child print accept", accept);
            revealAsset();
          })
          .catch((reject) => {
            console.log("child print reject", reject);
          });
      });
    } else {
      revealAsset();
    }
  }

  return (
    <div className="App">
      <img className="img" src={lock} alt="" />
      <div className="container"></div>

      <div className="Main">
        <h1>{config.settings.headline}</h1>
        <p>{config.settings.description}</p>
        <button className="class2" onClick={unblur}>
          Unlock (${Price})
        </button>
      </div>
    </div>
  );
};

export default App;
